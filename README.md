# haproxy-pgbouncer -> etcd-patroni-cluster

Запуск отказоустойчивого кластера PostgreSQL:

- HAproxy балансирует нагрузку между двумя менеджерами подключений pgbouncer
- Конфигурацией pgbouncer управялет демон confd
- Кластером PostgreSQL управляет Patroni
- Данные о состоянии кластера храняться в key-value хранилище etcd

## Getting started

Для развертывания кластера используйте плэйбук main.yaml
```
ansible-playbook main.yaml -b
```

Для развертывания системы мониторинга используйте плэйбук main.yaml
```
ansible-playbook monitoring.yaml -b
```

Чтобы перезапустить кластер etcd используйте плэйбук restart-services.yaml
```
ansible-playbook restart-services.yaml -b --tags=etcd
```

Чтобы перезапустить confd используйте плэйбук restart-services.yaml
```
ansible-playbook restart-services.yaml -b --tags=confd
```

![Cluster logo](https://gitlab.com/d6076/haproxy-etcd-pgbouncer-patroni/-/raw/main/patroni-ectd--pgbouncer-cluster.jpg?raw=true)
